# Changelog : DSFR Raccourcis Typographiques

Changelog du plugin SPIP **DSFR Raccourcis Typographiques**. Pour connaitre les changements concernant le DSFR vous
pouvez consulter son changelog ici : https://github.com/GouvernementFR/dsfr/blob/main/CHANGELOG.md


## 0.9.2 - 2024-10-03

### Fixed

- Regex de capture d'un raccourci
- Espacement du rendu d'un raccourci

### Changed

- Depuis la v1.12 du DSFR les tableaux ne peuvent plus avoir de couleur illustrative
- MAJ de la documentation


## 0.9.1 - 2024-07-19

### Fixed

- Plugin toujours en `dev` et non stable (fix du changement malvenu de @JamesRezo)


## 0.9.0 - 2024-07-05

### Fixed

- Paramètres du composant `citation`
- Espacement des alertes
- Utilisation du plugin `coloration_syntaxique`

### Added

- Compatibilité SPIP 4.*
  
### Changed

- Éviter les confusions `attribut_id`, `attribut_class`
- Prise en compte de l'écolution du plugin `coloration_syntaxique`
- Paramètres `source_details` du raccourci du composant `citation`



## 0.8.0 - 2024-06-06

### Added

- Des logs en cas d'erreur
- Des messages d'erreurs si les paramètres ne sont pas valides
- Fonctions du plugin surchargeables dans le dossier `dsfr`
- Traduction

### Changed

- Normalisation de la gestion des raccourcis typographiques
- Traitement des tableaux DSFR avec la fonction `dsfr_traitement()`


## 0.7.0 - 2024-05-28

### Added

- Raccourci typographique `<dsfr-accordeon>...</dsfr-accordeon>`
- Raccourci typographique `<dsfr-groupe_d_accordeons>...</dsfr-groupe_d_accordeons>`

### Changed

- Nom de la fonction de traitement d'un raccourci : `traiter_raccourci`
- Necessite le plugin `dsfr_composants` en version 2.1+


## 0.6.1 - 2024-05-24

### Fixed

- Lien de la documentation vers https://contrib.spip.net/DSFR-Raccourcis-Typographiques
- Force le type d'alerte si il est invalide
- Forcer la marge du premier raccourci typographique des exemples de code du plugin `dsfr_coloration_syntaxique`


## 0.6.0 - 2024-05-17

### Changed

- Référence de la surcharge du plugin dsfr_coloration_syntaxique qui a été renommé


## 0.5.0 - 2024-05-16

### Added

- Boutons des raccourcis typographiques dans le porte plume SPIP


## 0.4.0 - 2024-05-14

### Added

- Raccourci typographique `<dsfr-tableau>...</dsfr-tableau>`


## 0.3.0 - 2024-05-13

### Added

- Raccourci typographique `<dsfr-alerte>...</dsfr-alerte>`
- Raccourci typographique `<dsfr-citation>...</dsfr-citation>`
- Raccourci typographique `<dsfr-mise_en_avant>...</dsfr-mise_en_avant>`
- Raccourci typographique `<dsfr-mise_en_exergue>...</dsfr-mise_en_exergue>`


## 0.2.0 - 2024-05-07

### Changed

- Gestion du traitement des raccourcis typographique `<dsfr-`


## 0.1.0 - 2024-05-06

- Version initiale du plugin