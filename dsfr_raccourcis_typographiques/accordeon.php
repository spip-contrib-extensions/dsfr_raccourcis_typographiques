<?php
/**
 * Fonctions de raccourci typographique DSFR
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Configuration et description du raccourci typographique.
 * 
 * @return array
 **/
function raccourci_typographique_dsfr_accordeon() {
	return [
		'nom'			=> _T('dsfr_raccourcis_typographiques:accordeon'),
		'porte_plume'	=> [
			'openWith' => "\n<dsfr-accordeon|titre=Intitulé accordéon>\n",
			'closeWith' => "\n</dsfr-accordeon>\n",
		],
	];
}

/**
 * Traitement du raccourci typographique.
 * 
 * @param array $parametres
 * @param string $texte
 * 
 * @return array
 **/
function raccourci_typographique_dsfr_accordeon_traitement($parametres = [], $texte = '') {

	// gestion des erreurs
	$erreurs = [];
	if ( empty($parametres['texte']) ) {
		$erreurs[] = _T('dsfr_raccourcis_typographiques:le_raccourci_est_vide');
	}
	if ( empty($parametres['titre']) ) {
		$erreurs[] = _T('dsfr_raccourcis_typographiques:le_parametre_est_obligatoire', ['nom_du_parametre' => '<code>titre</code>']);
	}
	if ( empty(!$erreurs) ) {
		return ['erreur' => $erreurs];
	}

	// supprimer les paramètres invalides pour ne pas les transmettre au squelette
	$parametres = array_intersect_key($parametres, array_flip(['texte','titre']));

	// ajoute automatiquement le même espacement que les paragraphes de texte DSFR
	$parametres['attribut_class'] = 'fr-my-3w';

	// retour du traitement
	return ['raccourci' => recuperer_fond('dsfr_composants/accordeon', $parametres)];
}