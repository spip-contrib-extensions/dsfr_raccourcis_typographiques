<?php
/**
 * Fonctions de raccourci typographique DSFR
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Configuration et description du raccourci typographique.
 * 
 * @return array
 **/
function raccourci_typographique_dsfr_mise_en_exergue() {
	return [
		'nom'			=> _T('dsfr_raccourcis_typographiques:mise_en_exergue'),
		'porte_plume'	=> [
			'openWith' => "\n<dsfr-mise_en_exergue>\n",
			'closeWith' => "\n</dsfr-mise_en_exergue>\n",
		],
	];
}

/**
 * Traitement du raccourci typographique.
 * 
 * @param array $parametres
 * @param string $texte
 * 
 * @return array
 **/
function raccourci_typographique_dsfr_mise_en_exergue_traitement($parametres = [], $texte = '') {

	// gestion des erreurs
	$erreurs = [];
	if ( empty($parametres['texte']) ) {
		$erreurs[] = _T('dsfr_raccourcis_typographiques:le_raccourci_est_vide');
	}
	if ( empty(!$erreurs) ) {
		return ['erreur' => $erreurs];
	}

	// supprimer les paramètres invalides pour ne pas les transmettre au squelette
	$parametres = array_intersect_key($parametres, array_flip(['texte','taille','couleur']));

	// ajoute automatiquement le même espacement que les paragraphes de texte DSFR
	$parametres['attribut_class'] = 'fr-my-3w';

	// retour du traitement
	return ['raccourci' => recuperer_fond('dsfr_composants/mise_en_exergue', $parametres)];
}