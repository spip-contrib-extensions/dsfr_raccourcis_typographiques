<?php
/**
 * Fonctions de raccourci typographique DSFR
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Configuration et description du raccourci typographique.
 * 
 * @return array
 **/
function raccourci_typographique_dsfr_alerte() {
	return [
		'nom'			=> _T('dsfr_raccourcis_typographiques:alerte'),
		'porte_plume'	=> [
			'openWith' => "\n<dsfr-alerte>\n",
			'closeWith' => "\n</dsfr-alerte>\n",
		],
	];
}

/**
 * Traitement du raccourci typographique.
 * 
 * @param array $parametres
 * @param string $texte
 * 
 * @return array
 **/
function raccourci_typographique_dsfr_alerte_traitement($parametres = [], $texte = '') {

	// gestion des erreurs
	$erreurs = [];
	if ( empty($parametres['texte']) && empty($parametres['titre']) ) {
		$erreurs[] = _T('dsfr_raccourcis_typographiques:le_raccourci_est_vide');
	}
	if ( empty(!$erreurs) ) {
		return ['erreur' => $erreurs];
	}

	// force le type d'alerte si il est invalide
	if ( empty($parametres['type']) || !in_array($parametres['type'], ['erreur','succes','attention','information']) ) {
		$parametres['type'] = 'information';
	}
	
	// supprimer les paramètres invalides pour ne pas les transmettre au squelette
	$parametres = array_intersect_key($parametres, array_flip(['texte','titre','taille','type','afficher_croix_fermeture','texte_croix_fermeture','icone','balise_titre']));

	// ajoute automatiquement le même espacement que les paragraphes de texte DSFR
	$parametres['attribut_class'] = 'fr-my-3w';

	// retour du traitement
	return ['raccourci' => recuperer_fond('dsfr_composants/alerte', $parametres)];
}