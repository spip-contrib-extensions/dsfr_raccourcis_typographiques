<?php
/**
 * Fonctions de raccourci typographique DSFR
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Configuration et description du raccourci typographique.
 * 
 * @return array
 **/
function raccourci_typographique_dsfr_citation() {
	return [
		'nom'			=> _T('dsfr_raccourcis_typographiques:citation'),
		'porte_plume'	=> [
			'openWith' => "\n<dsfr-citation>\n",
			'closeWith' => "\n</dsfr-citation>\n",
		],
	];
}

/**
 * Traitement du raccourci typographique.
 * 
 * @param array $parametres
 * @param string $texte
 * 
 * @return array
 **/
function raccourci_typographique_dsfr_citation_traitement($parametres = [], $texte = '') {

	// gestion des erreurs
	$erreurs = [];
	if ( empty($parametres['texte']) ) {
		$erreurs[] = _T('dsfr_raccourcis_typographiques:le_raccourci_est_vide');
	}
	if ( empty(!$erreurs) ) {
		return ['erreur' => $erreurs];
	}

	if ( !empty($parametres['source_details']) ) {
		$parametres['source_details'] = array_map('trim',explode(',',$parametres['source_details']));
	}

	// supprimer les paramètres invalides pour ne pas les transmettre au squelette
	$parametres = array_intersect_key($parametres, array_flip(['texte','auteur','source','source_url','source_details','taille','couleur']));

	// ajoute automatiquement le même espacement que les paragraphes de texte DSFR
	$parametres['attribut_class'] = 'fr-mb-3w';

	// retour du traitement
	return ['raccourci' => recuperer_fond('dsfr_composants/citation', $parametres)];
}