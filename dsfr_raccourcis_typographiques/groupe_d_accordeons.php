<?php
/**
 * Fonctions de raccourci typographique DSFR
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Configuration et description du raccourci typographique.
 * 
 * @return array
 **/
function raccourci_typographique_dsfr_groupe_d_accordeons() {
	return [
		'nom'			=> _T('dsfr_raccourcis_typographiques:groupe_d_accordeons'),
		'porte_plume'	=> [
			'openWith' => "\n<dsfr-groupe_d_accordeons>\n",
			'closeWith' => "\n</dsfr-groupe_d_accordeons>\n",
		],
	];
}

/**
 * Traitement du raccourci typographique.
 * 
 * @param array $parametres
 * @param string $texte
 * 
 * @return array
 **/
function raccourci_typographique_dsfr_groupe_d_accordeons_traitement($parametres = [], $texte = '') {

	// gestion des erreurs
	$erreurs = [];
	if ( empty($parametres['texte']) ) {
		$erreurs[] = _T('dsfr_raccourcis_typographiques:le_raccourci_est_vide');
	}
	if ( empty(!$erreurs) ) {
		return ['erreur' => $erreurs];
	}

	// le squelette du composant attend une liste ou un tableau pour le paramètre `accordeons`
	$parametres['accordeons'] = [$parametres['texte']];
	
	// supprimer les paramètres invalides pour ne pas les transmettre au squelette
	$parametres = array_intersect_key($parametres, array_flip(['accordeons','fermeture_des_accordeons']));

	// ajoute automatiquement le même espacement que les paragraphes de texte DSFR
	$parametres['attribut_class'] = 'fr-my-3w';

	// retour du traitement
	return ['raccourci' => recuperer_fond('dsfr_composants/groupe_d_accordeons', $parametres)];
}