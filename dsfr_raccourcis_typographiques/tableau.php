<?php
/**
 * Fonctions de raccourci typographique DSFR
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Configuration et description du raccourci typographique.
 * 
 * @return array
 **/
function raccourci_typographique_dsfr_tableau() {
	return [
		'nom'			=> _T('dsfr_raccourcis_typographiques:tableau'),
		'porte_plume'	=> [
			'openWith' => "\n<dsfr-tableau>\n",
			'closeWith' => "\n</dsfr-tableau>\n",
		],
	];
}

/**
 * Traitement du raccourci typographique.
 * 
 * @param array $parametres
 * @param string $texte
 * 
 * @return array
 **/
function raccourci_typographique_dsfr_tableau_traitement($parametres = [], $texte = '') {

	// gestion des erreurs
	$erreurs = [];
	if ( empty($parametres['texte']) || stripos($parametres['texte'], '<table') === false ) {
		$erreurs[] = _T('dsfr_raccourcis_typographiques:le_raccourci_doit_contenir_un_tableau');
	}
	if ( empty(!$erreurs) ) {
		return ['erreur' => $erreurs];
	}

	$texte = $parametres['texte'];

	// supprimer les paramètres invalides pour ne pas les transmettre au squelette
	$parametres = array_intersect_key($parametres, array_flip(['afficher_titre','bordures_entre_chaque_ligne','defilement_horizontal','largeur_des_colonnes_identique']));

	// retour du traitement
	// implicitement une mise en conformité DSFR des `<table>` SPIP sera faite !
	return ['raccourci' => dsfr_traitement('mise_en_conformite_tableaux_spip', $texte, $parametres)];
}