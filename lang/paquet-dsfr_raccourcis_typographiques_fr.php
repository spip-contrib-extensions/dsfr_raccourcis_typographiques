<?php
/**
 * Fichier langue de SPIP
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * 
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'dsfr_raccourcis_typographiques_description' => '
Le plugin {{DSFR Raccourcis Typographiques}} ajoute pour vos contenus éditoriaux des raccourcis typographiques spécifiques au DSFR.
',
	'dsfr_raccourcis_typographiques_nom' => 'DSFR Raccourcis Typographiques',
	'dsfr_raccourcis_typographiques_slogan' => 'Raccourcis Typographiques SPIP pour le Système de Design de l\'État',
);