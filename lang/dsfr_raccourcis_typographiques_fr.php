<?php
/**
 * Fichier langue de SPIP
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * 
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'accordeon' => 'Accordéon - Accordion',
	'alerte' => 'Alerte - Alert',

	// C
	'citation' => 'Citation - Quote',

	// D
	'dsfr_raccourcis_typographiques' => 'DSFR Raccourcis Typographiques',

	// E
	'erreur_de_traitement_du_raccourci' => 'Erreur de traitement du raccourci !',

	// G
	'groupe_d_accordeons' => 'Groupes d\'accordéons - Accordions group',

	// I
	'inserer_des_raccourcis_typographiques_DSFR' => 'Insérer des raccourcis typographiques DSFR',

	// L
	'le_parametre_est_obligatoire' => 'Le paramètre @nom_du_parametre@ est obligatoire.',
	'le_raccourci_est_vide' => 'Le raccourci est vide.',
	'le_raccourci_doit_contenir_un_tableau' => 'Le raccourci doit contenir un tableau.',

	// M
	'mise_en_avant' => 'Mise en avant - Callout',
	'mise_en_exergue' => 'Mise en exergue - Highlight',

	// R
	'raccourci' => 'Raccourci',
	'raccourci_typographique_invalide' => 'Raccourci typographique invalide !',

	// T
	'tableau' => 'Tableau - Table',
);