<?php
/**
 * Fonctions du plugin surchargeables
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Charge la configuration/description d'un raccourci typographique disponible pour le DSFR
 * 
 * @param string $nom_du_raccourci
 * 
 * @return array|bool
 *     - array : configuration/description du raccourci
 *     - false : raccourci invalide/introuvable
 **/
function dsfr_raccourcis_typographiques_raccourci_dist($nom_du_raccourci) {

	if ( !$nom_du_raccourci ) {
		return false;
	}

	$nom = strtolower(trim($nom_du_raccourci));

	if ( empty($nom) ) {
		return false;
	}

	// déjà en cache ?
	if ( isset($GLOBALS['dsfr_raccourcis_typographiques_raccourci_'.$nom]) ) {
		return $GLOBALS['dsfr_raccourcis_typographiques_raccourci_'.$nom];
	}

	$raccourci = false;

	$prefix = 'raccourci_typographique_dsfr_'.$nom;

	// il y a un fichier php associé au raccourci ? 
	if ( find_in_path('dsfr_raccourcis_typographiques/'.$nom.'.php') ) {
		include_spip('dsfr_raccourcis_typographiques/'.$nom);
	}

	// fonction de traitement (prioritaire sur le squelette)
	if ( function_exists($prefix.'_traitement') ) {
		$traitement = $prefix.'_traitement';
	}
	// squelette -> remplacement du raccourci par le contenu du squelette
	else if ( trouver_fond($nom, 'dsfr_raccourcis_typographiques/') ) {
		$traitement = 'squelette';
	}
	else {
		$traitement = false;
	}

	// il y a un traitement disponible, on considère le raccourci comme valide et actif !
	if ( $traitement ) {
		$raccourci = [
			'id'			=> $nom,
			'prefix'		=> $prefix,
			'traitement'	=> $traitement,
		];

		// fonction de configuration 
		if ( function_exists($prefix) ) {
			$configuration = $prefix();

			if ( is_array($configuration) ) {
				$raccourci = array_merge($raccourci, $configuration);
			}
		}

		// active automatiquement le porte plume si il y a une icône associé
		if ( !isset($raccourci['porte_plume']) && find_in_path('dsfr_raccourcis_typographiques/'.$nom.'.svg') ) {
			$raccourci['porte_plume'] = [
				'openWith' => "\n<dsfr-".$nom.">\n",
			];
		}

		// configuration automatique du porte plume
		if ( isset($raccourci['porte_plume']) ) {
			if ( empty($raccourci['porte_plume']['id']) ) {
				$raccourci['porte_plume']['id'] = $prefix;
			}
			if ( empty($raccourci['porte_plume']['name']) ) {
				$raccourci['porte_plume']['name'] = !empty($raccourci['nom']) ? $raccourci['nom'] : $nom;
			}
			if ( empty($raccourci['porte_plume']['className']) ) {
				$raccourci['porte_plume']['className'] = 'outil_'.$prefix;
			}
		}
	}

	// stockage en $GLOBALS pour éviter de re-charger à chaque fois si il y a plusieurs appels à cette fonction
	$GLOBALS['dsfr_raccourcis_typographiques_liste'] = $raccourci;

	return $raccourci;
}

/**
 * Liste de tous les raccourcis typographiques disponibles pour le DSFR
 *  
 * @return array $raccourcis
 **/
function dsfr_raccourcis_typographiques_liste_dist() {

	// déjà en cache ?
	if ( isset($GLOBALS['dsfr_raccourcis_typographiques_liste']) ) {
		return $GLOBALS['dsfr_raccourcis_typographiques_liste'];
	}

	$fonction_dsfr_raccourcis_typographiques_raccourci = charger_fonction('raccourci', 'dsfr/raccourcis_typographiques');

	$raccourcis = [];
	foreach ( find_all_in_path('dsfr_raccourcis_typographiques/', '[.](php|html)$') as $chemin ) {
		$pathinfo = pathinfo($chemin);
		$nom = basename($chemin, '.'.$pathinfo['extension']);

		// déjà traité ?
		if ( array_key_exists($nom,$raccourcis) ) {
			continue;
		}

		if ( $raccourci = $fonction_dsfr_raccourcis_typographiques_raccourci($nom) ) {
			$raccourcis[$nom] = $raccourci;
		}
	}

	ksort($raccourcis);

	// stockage en $GLOBALS pour éviter de re-charger à chaque fois si il y a plusieurs appels à cette fonction
	$GLOBALS['dsfr_raccourcis_typographiques_liste'] = $raccourcis;

	return $raccourcis;
}

/**
 * Échappements de tous les raccourcis typographiques DSFR trouvés
 * correspondant au motif "regex" du nom du raccourci.
 *
 * @param string $texte
 * 
 * @param string $nom_du_raccourci
 *     Par défaut tous les raccourcis commençant par `<dsfr-` sont recherchés et traités.
 *     Vous pouvez cepandant cibler la recherche et le traitement d'un raccourci spécifique (ex. `accordeon`).
 * 
 * @return string
 *     Texte transformé (ou pas ^^)
 **/
function dsfr_raccourcis_typographiques_echapper_dist($texte, $nom_du_raccourci = '[a-z_-]{2,}+') {

	// pas de raccourcis !
	if ( !$texte || (stripos($texte, '<dsfr-') === false && stripos($texte, '</dsfr-') === false) ) {
		return $texte;
	}

	if ( preg_match_all('/<(\/)?dsfr-('.$nom_du_raccourci.')\s*([|]*(?:<[^<>]*>|[^>])*?)?\s*>/isS', $texte, $raccourcis_trouves, PREG_SET_ORDER) ) {
		foreach ( $raccourcis_trouves as $r ) {
			$r_position = strpos($texte, $r[0]);
			$r_longueur = strlen($r[0]);
			$r_echappe = code_echappement($r[0], 'dsfr_raccourcis_typographiques', true, 'div'); // utilise `div` pour éviter les `autobr`

			// remplacement du raccourci DSFR par un contenu échappé
			$texte = substr_replace($texte, $r_echappe, $r_position, $r_longueur);
		}
	}

	return $texte;
}

/**
 * Recherche et traitement de tous les raccourcis typographiques DSFR trouvés
 * correspondant au motif "regex" du nom du raccourci.
 *
 * @param string $texte
 * 
 * @param string $nom_du_raccourci
 *     Par défaut tous les raccourcis commençant par `<dsfr-` sont recherchés et traités.
 *     Vous pouvez cepandant cibler la recherche et le traitement d'un raccourci spécifique (ex. `accordeon`).
 * 
 * @return string
 *     Texte transformé (ou pas ^^)
 **/
function dsfr_raccourcis_typographiques_rechercher_et_traiter_dist($texte, $nom_du_raccourci = '[a-z_-]{2,}+') {

	// pas de raccourcis !
	if ( !$texte || (stripos($texte, '<dsfr-') === false && stripos($texte, '</dsfr-') === false) ) {
		return $texte;
	}

	// fonction de traitement spécifique d'un raccourci trouvé dans le texte
	$fonction_dsfr_raccourcis_typographiques_traiter_raccourci_trouve = charger_fonction('traiter_raccourci_trouve', 'dsfr/raccourcis_typographiques');

	/*
		traitements des raccourcis "encadrants"
	*/
	$inception_max = 7;
	while (
		strpos($texte, '<dsfr') !== false
		AND
//		preg_match_all('/<dsfr-(?P<nom_du_raccourci>'.$nom_du_raccourci.')\s*(?P<parametres>[|]*(?:<[^<>]*>|[^>])*?)?\s*>(?P<texte>(?:(?!<dsfr-(?&nom_du_raccourci))[\s\S])*)<\/dsfr-(?&nom_du_raccourci)>/UisS', $texte, $raccourcis_trouves, PREG_SET_ORDER)
		preg_match_all('/<dsfr-(?P<nom_du_raccourci>'.$nom_du_raccourci.')\s*(?P<parametres>[|]*(?:<[^<>]*>|[^>])*?)?\s*>(?P<texte>(?:(?!<dsfr-\k<nom_du_raccourci>)[\s\S])*)<\/dsfr-\k<nom_du_raccourci>>/UisS', $texte, $raccourcis_trouves, PREG_SET_ORDER)
		AND
		$inception_max--
	) {
		foreach ( $raccourcis_trouves as $raccourci ) {
			$texte = $fonction_dsfr_raccourcis_typographiques_traiter_raccourci_trouve($texte, $raccourci);
		}
	}

	/*
		traitements des raccourcis "non encadrant" (comme les modèles)
	*/
	$inception_max = 7;
	while (
		strpos($texte, '<dsfr') !== false 
		AND
		preg_match_all('/<dsfr-(?P<nom_du_raccourci>'.$nom_du_raccourci.')\s*(?P<parametres>[|]*(?:<[^<>]*>|[^>])*?)?\s*>/isS', $texte, $raccourcis_trouves, PREG_SET_ORDER)
		AND
		$inception_max--
	) {
		foreach ( $raccourcis_trouves as $raccourci ) {
			$texte = $fonction_dsfr_raccourcis_typographiques_traiter_raccourci_trouve($texte, $raccourci);
		}
	}

	// supprime tous les résidus de raccourcis mal formatés/fermés !
	$texte = preg_replace('/<\/?dsfr-('.$nom_du_raccourci.')\s*([|]*(?:<[^<>]*>|[^>])*?)?\s*>/isS', '', $texte);

	return $texte;
}

/**
 * Traitement spécifique d'un raccourci DSFR
 *
 * @param string $texte
 * 
 * @param array $raccourci_trouve Tableau retourné par la fonction de recherche :
 *  - index 0 : la capture du raccourci complet
 *  - index 1 : le nom du raccourci
 *  - index 2 : les paramètres du raccourci (comme les modèles SPIP)
 *  - index 3 : le contenu du texte compris entre les balises du raccourci (si c'est un raccourci "encadrant")
 * 
 * @return string
 *     Texte transformé (ou pas ^^)
 **/
function dsfr_raccourcis_typographiques_traiter_raccourci_trouve_dist($texte, $raccourci_trouve) {
	$position = strpos($texte, $raccourci_trouve[0]);
	
	// le raccourci n'est pas présent dans le texte ! (déjà traité avant ?)
	if ( $position === false ) {
		return $texte;
	}

	// longueur (pour faciliter le remplacement)
	$longueur = strlen($raccourci_trouve[0]);

	// nom du raccourci toujours en minuscule
	$nom = strtolower($raccourci_trouve[1]);

	// paramètres
	$parametres = array_filter(array_map('trim',explode('|', $raccourci_trouve[2]))); // trim et supprime les paramètres vide

	// contenu d'un raccourci encadrant ?
	$contenu = isset($raccourci_trouve[3]) ? trim($raccourci_trouve[3]) : '';

	// message d'erreur
	$erreur = false;

	// texte traité
	$texte_traite = null;

	// charge le raccourci et vérifie sa validité
	$fonction_dsfr_raccourcis_typographiques_raccourci = charger_fonction('raccourci', 'dsfr/raccourcis_typographiques');
	if ( $raccourci = $fonction_dsfr_raccourcis_typographiques_raccourci($nom) ) {

		// si il y a des paramètres, création du contexte (un tableau d'arguments comme SPIP le fait pour les modèles)
		$contexte = $parametres ? array_filter(array_map('trim',creer_contexte_de_modele($parametres))) : [];

		// ajoute le contenu au contexte
		if ( !empty($contenu) ) {
			$contexte = array_merge($contexte,['texte'=>$contenu]);
		}

		$retour_traitement = [
			'erreur'	=> false,
			'raccourci'	=> null,
			'texte'		=> null,
		];

		// squelette -> remplacement du raccourci
		if ( $raccourci['traitement'] == 'squelette' ) {
			$traitement_squelette = recuperer_fond('dsfr_raccourcis_typographiques/'.$raccourci['id'], $contexte);
			// le retour ne doit pas être vide
			if ( !empty($traitement_squelette) ) {
				$retour_traitement['raccourci'] = $traitement_squelette;
			}
			else {
				$retour_traitement['erreur'] = _T('dsfr_raccourcis_typographiques:erreur_de_traitement_du_raccourci');

				spip_log(
					"traitement dsfr-$nom : squelette vide ?",
					'dsfr_raccourcis_typographiques.' . _LOG_AVERTISSEMENT
				);
			}
		}
		// fonction de traitement (normalement déjà préchargée)
		else if ( function_exists($raccourci['traitement']) ) {
			$traitement_fonction = $raccourci['traitement']($contexte, $texte);
			if ( is_array($traitement_fonction) ) {
				foreach ( ['erreur','raccourci','texte'] as $retour_possible ) {
					if ( array_key_exists($retour_possible, $traitement_fonction) ) {
						$retour = $traitement_fonction[$retour_possible];
						// cas spécial pour `erreur` qui peut-être un tableau de messages d'erreurs
						if ( $retour_possible == 'erreur' && is_array($retour) ) {
							$retour = array_filter(array_map('trim',$retour));
						}
						else if ( is_string($retour) ) {
							$retour = trim($retour);
						}
						else {
							$retour = null; // retour invalide, on le considère comme `null`
						}

						if ( !is_null($retour) ) {
							$retour_traitement[$retour_possible] = $retour;
						}
					}
				}
			}

			// erreur automatique en fonction de certains retours invalides
			if ( !$retour_traitement['erreur'] ) {
				// trop de retour du traitement
				if ( !is_null($retour_traitement['raccourci']) && !is_null($retour_traitement['texte']) ) {
					$retour_traitement['erreur'] = _T('dsfr_raccourcis_typographiques:erreur_de_traitement_du_raccourci');

					spip_log(
						"traitement dsfr-$nom : retour `raccourci` ET `texte` NOT NULL",
						'dsfr_raccourcis_typographiques.' . _LOG_INFO_IMPORTANTE
					);
				}
				// aucun retour du traitement
				else if ( is_null($retour_traitement['raccourci']) && is_null($retour_traitement['texte']) ) {
					$retour_traitement['erreur'] = _T('dsfr_raccourcis_typographiques:erreur_de_traitement_du_raccourci');

					spip_log(
						"traitement dsfr-$nom : retour `raccourci` ET `texte` NULL",
						'dsfr_raccourcis_typographiques.' . _LOG_INFO_IMPORTANTE
					);
				}
				// retour texte ne doit pas être vide, sinon ça efface tout !
				else if ( !is_null($retour_traitement['texte']) && empty($retour_traitement['texte']) ) {
					$retour_traitement['erreur'] = _T('dsfr_raccourcis_typographiques:erreur_de_traitement_du_raccourci');

					spip_log(
						"traitement dsfr-$nom : retour `texte` vide",
						'dsfr_raccourcis_typographiques.' . _LOG_INFO_IMPORTANTE
					);
				}
			}
		}

		// erreur
		if ( $retour_traitement['erreur'] ) {
			$erreur = $retour_traitement['erreur'];
		}
		else if ( !is_null($retour_traitement['raccourci']) ) {
			$texte_traite = substr_replace($texte, $retour_traitement['raccourci'], $position, $longueur);
		}
		else if ( !is_null($retour_traitement['texte']) ) {
			$texte_traite = $retour_traitement['texte'];
		}
	}

	if ( !$erreur && is_null($texte_traite) ) {
		$erreur = _T('dsfr_raccourcis_typographiques:raccourci_typographique_invalide');
	}

	// le DSFR n'est pas disponible sur l'espace privé
	// remplace le raccourci par un message
	if ( test_espace_prive() ) {
		$remplacer  = '<div class="dsfr_raccourci_typographique" data-nom="dsfr-'.attribut_html($nom).'">';
		// il y a une erreur
		if ( $erreur ) {
			$remplacer  .= '<div class="dsfr_raccourci_typographique_erreur_message">';
			$remplacer  .= '<p>'.(is_array($erreur) ? implode('</p><p>',$erreur) : $erreur).'</p>';
			$remplacer  .= '</div>';
		}
		// il y a des paramètres
		if ( !empty($parametres) ) {
			$remplacer  .= '<div class="dsfr_raccourci_typographique_parametres">';
			$remplacer  .= spip_balisage_code('<dsfr-'.$nom.(!empty($parametres) ? '|'.implode('|',$parametres) : '').'>', true, '', 'spip');
			$remplacer  .= '</div>';
		}
		// il y a un contenu
		if ( !empty($contenu) ) {
			$remplacer .= '<div class="dsfr_raccourci_typographique_apercu">';
			$remplacer .= $contenu;
			$remplacer .= '</div>';
		}

		$remplacer .= '</div>';

		$texte = substr_replace($texte, $remplacer, $position, $longueur);
	}
	// espace public
	else {
		// il y a une erreur, on supprime le raccourci en gardant son contenu
		if ( $erreur ) {
			$texte = substr_replace($texte, $contenu, $position, $longueur);
		}
		else {
			$texte = $texte_traite;
		}
	}

	return $texte;
}