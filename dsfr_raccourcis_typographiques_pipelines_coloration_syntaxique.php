<?php
/**
 * Pipelines utilisés par le plugin
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * @pipeline coloration_syntaxique_css
 * 
 * @param string $flux
 * 
 * @return string
 **/
function dsfr_raccourcis_typographiques_coloration_syntaxique_css($flux) {

	// styles des raccourcis DSFR
	$flux['css/coloration_syntaxique_dsfr_raccourcis_typographiques.css'] = produire_fond_statique('css/coloration_syntaxique_dsfr_raccourcis_typographiques.css');

	return $flux;
}