/**
 * Extension de la grammaire PrismJS `spip` pour colorier les raccourcis typographiques DSFR
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
(function () {

	if ( typeof Prism === 'undefined' || typeof document === 'undefined' ) {
		return
	}

	if ( !Prism.languages.spip ) {
		return
	}

	// ouverture/fermeture des raccourcis sans paramètres
	Prism.languages.spip['dsfr-raccourci-typographique'] = {
		pattern: /<\/?dsfr-[a-zA-Z0-9_]+>/i,
		alias: 'tag dsfr-couleur-raccourci',
		inside: {
			'punctuation': {
				pattern: /^<\/?|>$/,
			},
		},
	}

	// modèle SPIP ayant un nom qui commence par `dsfr-`
	Prism.languages.spip['spip-modele'].inside['inclure'].inside['nom'].inside = {
		'dsfr': {
			pattern: /^dsfr-.*$/i,
			alias: 'dsfr-couleur-raccourci',
		},
	}
}())