<?php
/**
 * Pipelines utilisés par le plugin
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * @pipeline porte_plume_barre_pre_charger
 * 
 * @param array $barres
 * 
 * @return array
 **/
function dsfr_raccourcis_typographiques_porte_plume_barre_pre_charger($barres) {
	$fonction_dsfr_raccourcis_typographiques_liste = charger_fonction('liste', 'dsfr/raccourcis_typographiques');

	$dropMenu_DSFR = [];

	foreach ( $fonction_dsfr_raccourcis_typographiques_liste() as $raccourci ) {
		if ( !empty($raccourci['porte_plume']) ) {
			$dropMenu_DSFR[] = $raccourci['porte_plume'];
		}
	}

	// pas de boutons à précharger
	if ( empty($dropMenu_DSFR) ) {
		return $barres;
	}

	$barre = &$barres['edition'];

	$barre->ajouterPlusieursApres('grpCode', [
		[
			'id'			=> 'sepDSFR',
			'separator'		=> '---------------',
			'display'		=> true,
		],
		[
			'id'			=> 'grpDSFR',
			'name'			=> _T('dsfr_raccourcis_typographiques:inserer_des_raccourcis_typographiques_DSFR'),
			'className'		=> 'outil_dsfr',
			'display'		=> true,
			'dropMenu'		=> $dropMenu_DSFR,
		]
	]);

	return $barres;
}

/**
 * @pipeline porte_plume_lien_classe_vers_icone
 * 
 * @param array $flux
 * 
 * @return array
 **/
function dsfr_raccourcis_typographiques_porte_plume_lien_classe_vers_icone($flux) {
	$fonction_dsfr_raccourcis_typographiques_liste = charger_fonction('liste', 'dsfr/raccourcis_typographiques');

	$icones_DSFR = [];

	foreach ( $fonction_dsfr_raccourcis_typographiques_liste() as $id => $raccourci ) {
		if ( isset($raccourci['porte_plume']) && !empty($raccourci['porte_plume']['className']) ) {
			$icones_DSFR[$raccourci['porte_plume']['className']] = find_in_path('dsfr_raccourcis_typographiques/'.$id.'.svg');
		}
	}

	if ( !empty($icones_DSFR) ) {
		$icones_DSFR = array_merge(['outil_dsfr'=>find_in_path('dsfr_raccourcis_typographiques.svg')], $icones_DSFR);

		return array_merge($flux, $icones_DSFR);
	}

	return $flux;
}