<?php
/**
 * Pipelines SPIP utilisés par le plugin
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Échappements des raccourcis typographiques DSFR pour ne pas passer dans
 * la moulinette `echapper_html_suspect()` ni `traiter_modeles()` car les raccourcis
 * typographiques DSFR ressemblent aux raccourcis des modèles SPIP
 * 
 * @pipeline pre_echappe_html_propre
 * 
 * @param string $flux
 * 
 * @return string
 **/
function dsfr_raccourcis_typographiques_pre_echappe_html_propre($flux) {
	// pas de raccourcis !
	if ( !$flux || (stripos($flux, '<dsfr-') === false && stripos($flux, '</dsfr-') === false) ) {
		return $flux;
	}

	$fonction_dsfr_raccourcis_typographiques_echapper = charger_fonction('echapper', 'dsfr/raccourcis_typographiques');
	return $fonction_dsfr_raccourcis_typographiques_echapper($flux);
}

/**
 * Traitement des raccourcis typographiques DSFR
 * 
 * @pipeline post_propre
 * 
 * @param string $flux
 * 
 * @return string
 **/
function dsfr_raccourcis_typographiques_post_propre($flux) {
	// pas de raccourcis !
	if ( !$flux || strpos($flux, 'base64dsfr_raccourcis_typographiques') === false ) {
		return $flux;
	}

	// retransforme les raccourcis DSFR échappés
	$flux = echappe_retour($flux, 'dsfr_raccourcis_typographiques');

	$fonction_dsfr_raccourcis_typographiques_rechercher_et_traiter = charger_fonction('rechercher_et_traiter', 'dsfr/raccourcis_typographiques');
	return $fonction_dsfr_raccourcis_typographiques_rechercher_et_traiter($flux);
}